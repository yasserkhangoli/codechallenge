import React from "react";
import { render } from "@testing-library/react";
import Categories from "./index";

const testData = [
  {
    name: "Node 1",
    children: [
      { name: "Node 1.1", children: [] },
      {
        name: "Node 1.2",
        children: [
          { name: "Node 1.2.1", children: [] },
          { name: "Node 1.2.2", children: [] },
        ],
      },
    ],
  },
  { name: "Node 2", children: [] },
];

describe("Categories Tree Component", () => {
  test("renders the component without errors", () => {
    const { container } = render(<Categories categories={testData} />);
    expect(container.firstChild).toBeDefined();
  });

  test("renders the correct number of nodes", () => {
    const { getAllByTestId } = render(<Categories categories={testData} />);
    const nodes = getAllByTestId("node");
    expect(nodes.length).toBe(6);
  });
});
