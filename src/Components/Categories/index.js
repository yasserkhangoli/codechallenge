import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Typography,
} from "@mui/material";
import React from "react";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export default function Tree({ categories }) {
  return categories?.map((node) =>
    node.children.length > 0 ? (
      <Accordion
        data-testid="node"
        key={node.name}
        dir="rtl"
        variant="outlined"
      >
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>{node.name}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Tree categories={node.children} />
        </AccordionDetails>
      </Accordion>
    ) : (
      <Box
        data-testid="node"
        dir="rtl"
        sx={{
          height: 48,
          display: "flex",
          alignItems: "center",
          backgroundColor: "ButtonFace",
          cursor: "pointer",
        }}
      >
        {node.link}
      </Box>
    )
  );
}
