import React from "react";
import { Box, CircularProgress, Grid } from "@mui/material";
import Categories from "./Components/Categories";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchCategories } from "./Redux/Services/categories";

function App() {
  const dispatch = useDispatch();
  const categories = useSelector((store) => store.categories);

  useEffect(() => {
    dispatch(fetchCategories());
  }, []);

  return (
    <Grid justifyContent={"center"} container spacing={2}>
      {categories?.loading != "succeeded" ? (
        <CircularProgress
          data-testid="categories_circularProgress"
          color="secondary"
        />
      ) : (
        <Box data-testid="categories_box" sx={{ height: "100vh" }}>
          <Categories categories={categories?.list} />
        </Box>
      )}
    </Grid>
  );
}

export default App;
