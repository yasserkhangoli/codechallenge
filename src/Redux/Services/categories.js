import { createAsyncThunk } from "@reduxjs/toolkit";
import Axios from "../Helper/interceptor";

export const ENDPOINTS = {
  categories: "categories.json",
};

export const fetchCategories = createAsyncThunk(
  "users",
  async (obj, thukApi) => {
    try {
      const response = await Axios.get(ENDPOINTS.categories);
      return response.data;
    } catch (error) {
      return thukApi.rejectWithValue(error);
    }
  }
);
