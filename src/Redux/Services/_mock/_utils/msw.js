import { rest as mswRest } from "msw";

export const rest = {
  ...mswRest,
  get: (endpoint, resolver) =>
    mswRest.get(`${process.env.REACT_APP_BASE_URL}${endpoint}`, resolver),
};
