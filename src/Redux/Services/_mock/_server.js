import { setupServer } from "msw/node";

import handlers from "./categories.mock";

export const server = setupServer(...handlers);
