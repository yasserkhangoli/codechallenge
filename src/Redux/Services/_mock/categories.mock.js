import { rest } from "./_utils/msw";
import categoriesData from "./_data/categories.data";
import { ENDPOINTS } from "../categories";

const defualtHandlers = [
  rest.get(ENDPOINTS.categories, (req, res, ctx) => {
    return res(ctx.json(categoriesData));
  }),
];

export default defualtHandlers;
