import { configureStore } from "@reduxjs/toolkit";
import categoriesReducer from "../Slices/categories-slice";

export const store = configureStore({
  reducer: {
    categories: categoriesReducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware({ serializableCheck: false });
  },
});
