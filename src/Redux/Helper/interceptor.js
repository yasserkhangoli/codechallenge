import axios from "axios";

const instance = axios.create({ baseURL: process.env.REACT_APP_BASE_URL });

instance.interceptors.response.use(
  function (response) {
    console.log("axios interceptors response:: ", response);
    return response;
  },
  function (error) {
    console.log("axios interceptors response error:: ", error);
    return Promise.reject(error);
  }
);

const methods = {
  get: instance.get,
  post: instance.post,
  put: instance.put,
  delete: instance.delete,
  patch: instance.patch,
};

export default methods;
