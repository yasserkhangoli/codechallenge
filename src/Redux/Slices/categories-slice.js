import { createSlice } from "@reduxjs/toolkit";
import { fetchCategories } from "../Services/categories";

const initialState = {
  list: [],
  loading: "idle",
};

const categoriesSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchCategories.pending, (state, action) => {
        state.loading = "pending";
      })
      .addCase(fetchCategories.rejected, (state, action) => {
        state.loading = "failed";
      })
      .addCase(fetchCategories.fulfilled, (state, action) => {
        state.loading = "succeeded";
        state.list = action.payload;
      });
  },
});

export default categoriesSlice.reducer;
