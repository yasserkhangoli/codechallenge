import React from "react";
import { screen, waitFor } from "@testing-library/react";
import App from "./App";
import "@testing-library/jest-dom";
import { server } from "./Redux/Services/_mock/_server";
import { ENDPOINTS } from "./Redux/Services/categories";
import data from "./Redux/Services/_mock/_data/categories.data";
import { rest } from "./Redux/Services/_mock/_utils/msw";
import { renderWithProviders } from "./Utils/test_utils";

beforeAll(() => server.listen());

afterEach(() => server.resetHandlers());

afterAll(() => server.close());

test("renders learn react link", () => {
  renderWithProviders(<App />);

  server.use(
    rest.get(ENDPOINTS.categories, (req, res, ctx) => {
      return res.once(ctx.json(data), ctx.delay(150));
    })
  );

  expect(
    screen.getByTestId(/categories_circularProgress/i)
  ).toBeInTheDocument();

  waitFor(() =>
    expect(screen.getAllByText("categories_box")).toBeInTheDocument()
  );
});
